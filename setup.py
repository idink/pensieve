from setuptools import setup, find_packages

def readme():
    with open('./README.md') as f:
        return f.read()

setup(
    name='pensieve',
    version='2.0.0',
    license='GNU AGPLv3',

    author='Idin',
    author_email='d@idin.net',

    keywords='graph computation',
    description='Implementation of a computation graph',
    long_description=readme(),
    long_description_content_type='text/markdown',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ],

    packages=find_packages(exclude=("jupyter_tests", ".idea", ".git")),
    install_requires=['toposort', 'graphviz', 'memoria'],
    python_requires='~=3.6',
    zip_safe=True,
    test_suite='nose.collector',
    tests_require=['nose', 'coverage']
)
